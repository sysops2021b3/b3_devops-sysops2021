# Use official python runtime base image
FROM python

# Set the container application directory to /app
WORKDIR /app

# Copy the code from the current folder to /app inside the container
COPY . /app

# Install the requirements that are in requirements.txt - i.e Flask and Redis
# RUN pip install -r requirements.txt

# Make port 80 available for links and/or publish
EXPOSE 80

# Define the command to be run when launching the container : `python app.py`
CMD ["python", "main.py"]
