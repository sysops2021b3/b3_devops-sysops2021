import random
import time

tempsResalisationPizza = random.randint(10,15)
timer = 30 #NE PAS OUBLIER DE CHANGER A 360
i=0
fourUtilisatable = 0

pizza = ["hawaïenne", "rucola"]
pizzaCommande = []

class Four:
    occuper = False
    cuisson  = 10 # MODIFIER

Four = Four()

def main(i):
    score = 0
    nombreDePizza = 0
    ananas = 0
    jambon = 0
    roquette = 0
    afficheInstruction()
    print()

    while i < timer:
        time.sleep(1)
        i = i +1
        print("La commande est : ", commande(), end='\n')
        intOk = True
        print("Nombre de base de pizza : ", nombreDePizza)
        print("Nombre de morceaux de ananas : ", ananas)
        print("Nombre de morceaux de jambon : ", jambon)
        print("Nombre de morceaux de roquette : ", roquette, "\n")
        while intOk:
            try:
                nombreDePizza = int(input("Combien voulez de base de pizzas ? : "))
                intOk = False
            except ValueError:
                print("Merci d'entrer un CHIFFRE ou un NOMBRE !")
        ananas, jambon, roquette = nouvellePizza(ananas, jambon, roquette)
        
        if Four.occuper == False:
            Four.occuper = True
            if i < Four.cuisson:
                if calculQuantite : 
                    ananas, jambon, roquette, nombreDePizza, score = cuisson(ananas, jambon, roquette, nombreDePizza, score)
                    i+=Four.cuisson
                    Four.occuper = False
                    i += tempsResalisationPizza
            else: 
                i += timer-i #Mastermind
                print("Vous n'aviez pas les ingrédients nécéssaires pour la réalisation de la commande ou pas assez de temps")
                print("Vous revenez donc à l'écran de sélection des ingrédients avec une nouvelle commande \n")

        pizzaCommande.clear()
      
        print("Temps restant : ", timer - i, "secondes")
    print("Le temps est écoulé chef !")
    score = calcul(ananas, jambon, roquette, nombreDePizza, score)
    print("Vous avez réalisé ", score, "points")
    
    print("---------------------------------------------------------")
    intOk = True
    while intOk:
        try:
            relancer = int(input("Voulez vous relancer une partie ? (1 pour oui 0 pour non) "))
            intOk = False
        except ValueError:
            print("Merci d'entrer un CHIFFRE ou un NOMBRE !")
    if relancer == 1:
        main(0)
    else:
        print("Merci d'avoir joué au jeu !")
        time.sleep(5)
        main(0)

def cuisson(ananas, jambon, roquette, nombreDePizza, score):
    Four.occuper = True
    time.sleep(Four.cuisson)
    for y in pizzaCommande:
        if y == "rucola":
            roquette-=7
            nombreDePizza-=1
            
        if y == "hawaïenne":
            ananas-=3
            jambon-=3
            nombreDePizza-=1
    score+=10
    return ananas, jambon, roquette, nombreDePizza, score


def afficheInstruction(): 
    intro = ["Bonjour bienvenue dans le jeu de création de pizzas !",
             "Dans le temps impartis (6 min), vous devez remplir le plus de commandes possible",
             "Seulement 2 choix sont possible : Pizza rucola ou bien pizza hawaïenne",
             "Le temps pour réaliser une pizza varie entre 10 et 15 sec",
             "Le temps de cuisson est de 30 sec, et le nombre de pizza par fournée est de 3 au maximum",
             "Chaque ingrédient non utilisé sera décompté du résultat final",
             "La roquette doit être ajoutée après la cuisson",
             "Par pizza, il y a 3 ingrédients (par ingrédient)"]
    
    for i in range(len(intro)):
        print(intro[i])


def nouvellePizza(ananas, jambon, roquette):
    intOk = True
    while intOk:
        try:
            ananas += int(input("Combien voulez d'ananas ? : "))            
            jambon += int(input("Combien voulez de morceau de jambon ? : "))
            roquette += int(input("Combien voulez de morceau de roquette ? : "))
            print("")
            intOk = False
        except ValueError:
            print("Merci d'entrer un CHIFFRE ou un NOMBRE !")
    return ananas, jambon, roquette

def commande():
    for i in range(3):
        randomPizza = random.randint(0, 100) % 2
        pizzaCommande.append(pizza[randomPizza])
    return pizzaCommande

def calcul(ananas, jambon, roquette, nombreDePizza, score):
    #print("Pizza = 10 points, ingrédient en trop = -1, base en trop = -4")
    score = score - (ananas + jambon + roquette)
    score = score - (nombreDePizza * 4)
    return score

def calculQuantite(ananas, jambon, roquette, nombreDePizza):
    commandeRealisable = True
    totalPizzaRucola = 0
    totalPizzaHawaienne = 0
    for y in pizzaCommande:
        if y == "rucola":
            totalPizzaRucola+=1
        if y == "hawaïenne":
            totalPizzaHawaienne+=1
            
    if totalPizzaHawaienne + totalPizzaRucola > nombreDePizza:
        commandeRealisable = True
        
        if totalPizzaHawaienne >= 1 and (((totalPizzaHawaienne*3) <= ananas) and ((totalPizzaHawaienne*3) <= jambon)):
            commandeRealisable = True
        else: 
            return False
            
        if totalPizzaRucola >= 1 and ((totalPizzaRucola*7) <= roquette):
            commandeRealisable = True
        else: 
            commandeRealisable = False
            
    return commandeRealisable


main(i)
